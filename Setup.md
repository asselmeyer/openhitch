# Setting up a development environment

## IDEs
* Install Intellij, community edition is ok (for the server part)
  * In Intellij, go to Settings->Build,Execution,Deployment->Compiler->Kotlin Compiler
  * set Target JVM version to 1.8
* Install Android Studio (for the client part)
* Install Maven

## Repositories
* Clone all repositories from https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch, i.e.
  ```
  git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/openhitch.git && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/graphhopper.git && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/graphhopper-navigation.git && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/openhitch-protocol.git && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/graphhopper-openhitch.git && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/geocoder-converter.git && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/openhitch-android.git
  ```

* Check out specific branches 
  ```
  cd graphhopper
  git checkout openhitch
  cd ../graphhopper-navigation
  git checkout openhitch
  ```

  (There are newer versions in this repository, that do not correspond with the versions in the other repositories)


## Please Help
Open openhitch/Setup.md in a text editor and note all difficulties you have and how you dealt with them. Upload when you are done

## Protocol Definition
* Open Intelij
* Open Project openhitch-protocol
* specify java 1.8 as project SDK (right click project -> Open Module Settings -> Modules -> Dependencies)
* Open Maven Tool Window by clicking the Maven-Button at the right edge of the IDE and doubleclick "OpenHitch Protocol -> Lifecycle -> install"
  (alternatively start `mvn install` in a terminal)
  This will install the protocol definition in the local Maven central.

## GraphHopper/OpenHitch Server 
* With Intellij open project graphhopper
* Add graphhopper-navigation and graphhopper-openhitch as modules
  (click "File->New->Module from Existing Sources", select module directory, select "Import module from exernal model", select "Maven".
  Do update Kotlin compiler to 1.4.10, when asked).
* Click "Build->Build Project"
* From the command line run
  ```
  cd graphhopper-openhitch
  mvn install -DskipTests
  cd graphhopper-navigation
  mvn install -DskipTests
  cd ../graphhopper
  mvn install -DskipTests
  ./graphhopper.sh -a web -i europe_germany_berlin.pbf
  ```
  Then press Enter as prompted. This should download europe_germany_berlin.pbf create the graph and do some initial praparations.
  When it says "org.eclipse.jetty.server.Server - Started", stop it by pressing Ctrl-c. Now we start the server from the IDE as follows.
* Click "Run->Edit Configurations"
* Click "+" to add new configuration
* A menu pops up, choose Application
* Fill the following values into the form
  * Name: Web Server
  * Main class: com.graphhopper.http.GraphHopperApplication
  * VM options: -Xms1g -Xmx1g -server -Dgraphhopper.datareader.file=europe_germany_berlin.pbf -Dgraphhopper.graph.location=./europe_germany_berlin.osm-gh
  * Program arguments: server config.yml
  * Working directory: /xyz/graphhopper (replace xyz by the path where your graphhopper directory is)
  * Use classpath of module: graphhopper-web
  * JRE: 1.8
  
  Keep defaults for all fields not mentioned here
* Click "Run->Web Server"


### OpenMapTiles (Map server)
* Install docker
* Install and start the OpenMapTiles docker container:

  `sudo docker run --rm -it -v $(pwd):/data -p 8080:80 klokantech/openmaptiles-server`

* point browser to http://localhost:8080 , OpenMapTiles installation wizard should show up
* Go through wizard, choose
  * region: Germany/Berlin
  * style: OSM Bright
  * settings: Serve vector map tiles
  * Follow instructions on how to get a key and download the data

## Build and start Graphhopper Geocoder Converter (relays geo search to Nominatim)
* `cd geocoder-converter`
* `mvn package -DskipTests`
* `java -jar target/graphhopper-geocoder-converter-0.2-SNAPSHOT.jar server converter.yml`

## Install Mosquitto (MQTT broker)
* On debian, this can be done with

  `sudo apt install mosquitto`

## App
* Open Android Studio
* "Click File->New->Import Project", navigate to directory openhitch-android
  (Android Studio should start a Gradle sync. If not so, click "File->Synchronize Project" with gradle files)
* Click "Build->Make Project"
* Open "Tools->AVD Manager" and configure a device with API 29
  (In theory we support api level >= 23, but 23 currently crashes. Need to migrate to androidx).
* Start device and set location to some point in Berlin, Germany (click on the three dots in the toolbar for the device).
* Make shure the Wifi
* The language of the virtual device may be set to Englisch or German.
* Duplicate the device (Click "Duplicate" in it's drop down menu in the rightmost column of the AVD Manager).
* At the top of the main window of Android Studio, there is a drop down list of devices. You can open it and click "Run on multiple devices" to install and run the app on both devices simultaneously.
* In the app, set the user name to "max", "moritz" or "hans" (as there is no signup and authentication yet, these are hardcoded into the database).
  To do this, go to the second screen of the app by pressing "Offer ride". In the top menu, you find a gear wheel symblos to enter the settings menu.
  Press "User Name" and enter the name. This is only needed the first time, the information survices a reinstall of the app.
